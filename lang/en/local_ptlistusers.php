<?php
defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'Prueba Técnica';
$string['lista_usuarios'] = 'Lista de usuarios';
$string['userslist']  = 'Lista de usuarios';
$string['noresults'] = 'Sin resultados';
$string['errorreadingdatabase'] = 'Sin resultados';
$string['viewreport'] = 'Lista de usuarios';
$string['ptlistusers:viewreport'] = 'ver Reporte de Lista de Usuarios';
