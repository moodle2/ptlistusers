<?php
defined('MOODLE_INTERNAL') || die(); // Verifica que el script se está ejecutando dentro de Moodle, de lo contrario, termina la ejecución

if ($hassiteconfig) { // Comprueba si el usuario tiene permisos de configuración del sitio
    $settings = new admin_settingpage('localplugins', get_string('pluginname', 'local_ptlistusers')); // Crea una nueva página de configuración del plugin
    $ADMIN->add('localplugins', new admin_externalpage('local_ptlistusers',
        get_string('pluginname', 'local_ptlistusers'), // Nombre del plugin
        new moodle_url('/local/ptlistusers/users.php') // URL de la página del plugin
    ));

    $ADMIN->add('root', new admin_category('testTecnical', get_string('pluginname', 'local_ptlistusers'))); // Añade una nueva categoría de administración

    $ADMIN->add('localplugins', new admin_externalpage('local_ptlistusers',
        get_string('pluginname', 'local_ptlistusers'), // Nombre del plugin
        new moodle_url('/local/ptlistusers/users.php'), // URL de la página del plugin
        'local/ptlistusers:manage' // Capacidad requerida para acceder a esta página
    ));
    
    if ($ADMIN->fulltree) {
        // TODO: Define actual plugin settings page and add it to the tree - {@link https://docs.moodle.org/dev/Admin_settings}.
    }
}