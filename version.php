<?php
defined('MOODLE_INTERNAL') || die();
$plugin->component = 'local_ptlistusers';
$plugin->release = '0.1.0';
$plugin->version = 2024060100;
$plugin->requires = 2022112800;
$plugin->maturity = MATURITY_ALPHA;
