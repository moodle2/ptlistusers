# Prueba Técnica

## Instalación de Moodle

##### Versión Instalada

• Moodle 4.1
• PHP 7.4
• MariaDB 10.4

## Theme

• Boost

## Desarrollo del Plugin

Nombre del Plugin
• ptlistusers / Prueba Técnica

### Instalación y configuración

##### Ubicar el directorio

`\moodle\lib\classes\navigation\output\primary.php`

En la función get_custom_menu, se agregaron las siguientes instrucciones:

A continuación, el código a agregar, este debe ser añadido inmeditamente inicie la función:

```
PHP
$systemcontext = \context_system::instance();
$file_path = $CFG->dirroot . '/local/ptlistusers/users.php'; // Ruta absoluta del archivo
        if (file_exists($file_path)) {
            if( isloggedin() ){
                if(has_capability('moodle/role:manage', $systemcontext) || has_capability('moodle/role:admin', $systemcontext)){
                    $CFG->custommenuitems = get_string('pluginname', 'local_ptlistusers')." | /local/ptlistusers/users.php";
                }
            }
        }

```

##### Descripción de Funciones:

• \context_system::instance(): Obtiene el contexto del sistema.
• isloggedin(): Verifica si el usuario está logueado.
• has_capability(): Verifica si el usuario tiene una capacidad específica en un contexto dado.
• get_string(): Obtiene una cadena traducida del archivo de idioma especificado.

## Instalación del Plugin de Moodle

##### Descargar el Plugin

• Ve al directorio oficial de plugins de Moodle o al repositorio específico del plugin.
• Descarga el archivo ZIP del plugin.
• Subir el Plugin a Moodle
• Accede a tu sitio Moodle como administrador.
• Navega a Administración del sitio > Plugins > Instalar plugins.
• Sube el archivo ZIP del plugin descargado previamente.
• Haz clic en "Instalar plugin desde el archivo ZIP".
