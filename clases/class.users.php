<?php
class local_lista_usuarios {
    private $subTitle; // Subtítulo de la página
    private $pageparams; // Parámetros de la página
    private $context; // Contexto de la página
    private $table; // Tabla de usuarios
    private $on_download; // Indicador de si se está descargando el reporte

    public function __construct() {
        self::val_user(); // Valida el usuario
        $this->subTitle = get_string('lista_usuarios', 'local_ptlistusers'); // Establece el subtítulo
        $this->on_download = false; // Inicializa la variable de descarga
        self::display_page(); // Muestra la página
    }

    private function val_user() {
        $systemcontext = \context_system::instance(); // Obtiene el contexto del sistema
        if (isloggedin()) { // Verifica si el usuario está logueado
            if (!has_capability('moodle/role:manage', $systemcontext)) { // Verifica si el usuario tiene la capacidad requerida
                redirect(new moodle_url('/')); // Redirige al usuario si no tiene la capacidad
            }
        }
    }

    private function display_page() {
        global $OUTPUT; // Utiliza la salida global de Moodle
        self::setup_table(); // Configura la tabla
        $reportname = get_string('lista_usuarios', 'local_ptlistusers'); // Nombre del reporte
        $formato = optional_param('download', '', PARAM_ALPHA); // Obtiene el parámetro de formato de descarga

        if (!$this->table->is_downloading($formato, $reportname)) { // Verifica si no se está descargando el reporte
            echo $OUTPUT->header(); // Muestra el encabezado
            echo "<label>$this->subTitle</label>"; // Muestra el subtítulo
            try {
                $this->on_download = false; // Establece que no se está descargando
                $this->make_table(); // Crea la tabla
                $this->table->finish_output(); // Finaliza la salida de la tabla
            } catch (Exception $e) {
                echo $OUTPUT->notification(get_string('errorreadingdatabase', 'local_ptlistusers'), 'error'); // Muestra un error si ocurre una excepción
            }
            echo $OUTPUT->footer(); // Muestra el pie de página
        } else {
            try {
                $this->on_download = true; // Establece que se está descargando
                $this->make_table(); // Crea la tabla
                if ($_GET["download"] != "html") {
                    $this->table->finish_output(); // Finaliza la salida de la tabla si el formato no es HTML
                }
            } catch (Exception $e) {
                echo $OUTPUT->notification(get_string('errorreadingdatabase', 'local_ptlistusers'), 'error'); // Muestra un error si ocurre una excepción
            }
        }
    }

    private function setup_table() {
        $this->table = new flexible_table('tableblut'); // Crea una nueva tabla flexible
        $this->table->define_columns(array('username', 'firstname', 'lastname', 'email', 'course')); // Define las columnas de la tabla
        $this->table->define_headers(array(
            get_string('username', 'core'), // Encabezado de la columna "username"
            get_string('firstname', 'core'), // Encabezado de la columna "firstname"
            get_string('lastname', 'core'), // Encabezado de la columna "lastname"
            get_string('email', 'core'), // Encabezado de la columna "email"
            get_string('course', 'core') // Encabezado de la columna "course"
        ));
        $this->table->column_style('username', 'width', '15%'); // Estilo de la columna "username"
        $this->table->column_style('firstname', 'width', '15%'); // Estilo de la columna "firstname"
        $this->table->column_style('lastname', 'width', '15%'); // Estilo de la columna "lastname"
        $this->table->column_style('email', 'width', '15%'); // Estilo de la columna "email"
        $this->table->column_style('coursename', 'width', '40%'); // Estilo de la columna "coursename"
        $this->table->define_baseurl('http://localhost:3000/local/ptlistusers/users.php'); // URL base de la tabla
        $this->table->set_attribute('class', 'admintable blockstable generaltable'); // Atributos de clase de la tabla
        $this->table->set_attribute('id', 'tableblutx'); // Atributo ID de la tabla
        // $this->table->sortable(true); // Habilita la capacidad de ordenar la tabla (comentado)
        $this->table->is_downloadable(true); // Habilita la descarga de la tabla
        $this->table->show_download_buttons_at(array(TABLE_P_BOTTOM)); // Muestra los botones de descarga en la parte inferior
        // $this->table->collapsible(true); // Habilita la capacidad de colapsar la tabla (comentado)
        $this->table->setup(); // Configura la tabla
    }

    private function make_table() {
        global $DB, $OUTPUT; // Utiliza las variables globales de base de datos y salida
        $sql = "SELECT DISTINCT
                    u.username,
                    u.firstname,
                    u.lastname,
                    u.email,
                    GROUP_CONCAT(DISTINCT c.fullname SEPARATOR '<xd>') AS courses 
                FROM
                    {user} AS u
                    JOIN {role_assignments} AS ra ON u.id = ra.userid
                    JOIN {context} AS ctx ON ra.contextid = ctx.id
                    JOIN {course} AS c ON ctx.instanceid = c.id 
                WHERE
                    ctx.contextlevel = 50 AND u.id NOT IN (1, 2)
                GROUP BY
                    u.id"; // Consulta SQL para obtener los usuarios y sus cursos
        $users_with_courses = $DB->get_records_sql($sql); // Obtiene los registros de la base de datos
        if (empty($users_with_courses)) {
            $this->table->add_data([$OUTPUT->notification(get_string('noresults', 'local_ptlistusers'), 'info')]); // Agrega un mensaje si no hay resultados
        } else {
            if ($this->on_download === true) {
                foreach ($users_with_courses as $user) {
                    $courses = implode(",", explode("<xd>", $user->courses)); // Convierte los cursos a una cadena separada por comas
                    $this->table->add_data([$user->username, $user->firstname, $user->lastname, $user->email, $courses]); // Agrega los datos del usuario a la tabla
                }
            } else {
                foreach (self::config_page_table($users_with_courses) as $user) {
                    $courselist = '<ul>';
                    $courses = explode("<xd>", $user->courses); // Convierte los cursos a una lista
                    foreach ($courses as $course) {
                        $courselist .= '<li>' . $course . '</li>'; // Agrega cada curso a la lista
                    }
                    $courselist .= '</ul>';
                    $this->table->add_data([$user->username, $user->firstname, $user->lastname, $user->email, $courselist]); // Agrega los datos del usuario a la tabla
                }
            }
        }
    }

    private function config_page_table($users_with_courses) {
        $total_users = count($users_with_courses); // Cuenta el total de usuarios
        $users_for_pages = 5; // Define la cantidad de usuarios por página
        $this->table->pagesize($users_for_pages, $total_users); // Configura el tamaño de página de la tabla
        $start_row = $this->table->get_page_start(); // Obtiene el inicio de la fila de la página actual
        $end_row = $start_row + $users_for_pages; // Calcula el final de la fila de la página actual
        $current_page_data = array_slice($users_with_courses, $start_row, $users_for_pages); // Obtiene los datos de la página actual
        return $current_page_data; // Retorna los datos de la página actual
    }
}
?>