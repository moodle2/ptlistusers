<?php 
$capabilities = array(
    'local/ptlistusers:viewreport' => array( // Define una capacidad personalizada para ver el reporte
        'riskbitmask' => RISK_CONFIG, // Indica el riesgo asociado con esta capacidad
        'captype' => 'write', // Especifica el tipo de capacidad (lectura/escritura)
        'contextlevel' => CONTEXT_SYSTEM, // Nivel de contexto donde se aplica esta capacidad (sistema)
        'archetypes' => array(
            'manager' => CAP_ALLOW, // Permite esta capacidad a los usuarios con el rol de "manager"
            'admin' => CAP_ALLOW, // Permite esta capacidad a los usuarios con el rol de "admin"
        ),
    )
);