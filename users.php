<?php
require('../../config.php'); // Carga la configuración principal de Moodle.
global $CFG, $PAGE, $SITE; // Declara las variables globales necesarias.
require_once($CFG->libdir.'/adminlib.php'); // Incluye la librería de administración de Moodle.
require_once($CFG->libdir.'/tablelib.php'); // Incluye la librería de tablas de Moodle.
include_once('./clases/class.users.php'); // Incluye la clase personalizada de usuarios.
require_login(); // Requiere que el usuario esté logueado.
$PAGE->set_url('/local/ptlistusers/users.php'); // Establece la URL de la página.
$PAGE->set_title(get_string('pluginname', 'local_ptlistusers')); // Establece el título de la página.
$PAGE->set_heading(get_string('pluginname', 'local_ptlistusers')); // Establece el encabezado de la página.
new local_lista_usuarios(); // Crea una nueva instancia de la clase local_lista_usuarios.
 